package com.netcetera.restdemo.services;

import com.netcetera.restdemo.models.Person;
import org.springframework.stereotype.Service;

import java.util.*;

import static java.util.Optional.ofNullable;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Service
public class PersonService {
  private final Map<Long, Person> persons = new HashMap<>(Map.of(
      1L, new Person(1L, "hristijan"),
      2L, new Person(2L, "Dimitar")
  ));

  public List<Person> findAll() {
    return new ArrayList<>(persons.values());
  }

  public Optional<Person> findById(Long id) {
    return ofNullable(persons.get(id));
  }

  public Optional<Person> update(Long id, Person person) {
    if (Objects.isNull(persons.get(id))) {
      return empty();
    }

    persons.put(id, person);

    return of(person);
  }

  public Person add(Person person) {
    long nextId = persons.size() + 1;
    Person newPerson = new Person(nextId, person.getFirstName());
    persons.put(nextId, newPerson);
    return newPerson;
  }

}
