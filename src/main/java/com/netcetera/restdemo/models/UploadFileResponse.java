package com.netcetera.restdemo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class UploadFileResponse {
  @Getter
  private String fileName;
  @Getter
  private String fileDownloadPath;
  @Getter
  private String fileType;
  @Getter
  private Long size;
}
