package com.netcetera.restdemo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class Message {
  @Getter
  @Setter
  private String from;
  @Getter
  @Setter
  private String text;
}
