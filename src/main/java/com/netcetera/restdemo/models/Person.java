package com.netcetera.restdemo.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@AllArgsConstructor
public class Person {
  @Getter
  @Setter
  private Long id;
  @Getter
  @Setter
  @NotBlank
  private String firstName;
}
