package com.netcetera.restdemo.controllers;

import com.netcetera.restdemo.models.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class MessageController {

  @MessageMapping("/message")
  @SendTo("/topic/messages")
  public Message greeting(Message message) {
    return message;
  }
}
