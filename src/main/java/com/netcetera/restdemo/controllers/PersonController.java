package com.netcetera.restdemo.controllers;

import com.netcetera.restdemo.models.Person;
import com.netcetera.restdemo.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
public class PersonController {

  @Autowired
  private PersonService personService;

  @GetMapping("/persons")
  public List<Person> persons() {
    return personService.findAll();
  }

  @PostMapping("/persons")
  public ResponseEntity<Person> addPerson(@RequestBody @Valid Person person) {
      Person newPerson = personService.add(person);
      return ResponseEntity.status(HttpStatus.CREATED).body(newPerson);
  }

  @GetMapping("/persons/{id}")
  public ResponseEntity<Person> person(@PathVariable Long id) {
    return ResponseEntity.of(personService.findById(id));
  }

  @PutMapping("persons/{id}")
  public ResponseEntity<Person> updatePerson(@PathVariable Long id, @RequestBody @Valid Person person) {
    return ResponseEntity.of(personService.update(id, person));
  }

}
