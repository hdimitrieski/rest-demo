/**
 * Records:
 * Person:  {id: NUMBER, firstName: STRING}
 * File:    {fileName: STRING, fileDownloadPath: STRING, fileType: STRING, size: LONG}
 *
 * REST URLs
 * GET    persons       - Fetch all persons
 * POST   persons       - Create new person (body: Person)
 * GET    persons/:id   - Fetch person by id
 * PUT    persons/:id   - Update single person (body: Person)
 * DELETE persons/:id   - Delete person by id
 *
 * POST   uploadFile    - Upload a file (body: FormData[file])
 *
 * The content type that is used to send/retrieve data is: "application/json".
 *
 */

let selectedPerson = null;
let allPersons = [];
const allPersonsElement = document.querySelector('#allPersons');
const firstNameInput = document.querySelector('#firstName');

const renderPersons = (persons) => {
  allPersonsElement.innerHTML = '';

  const personsHtml = persons.map(person => `
    <div>
        <span>${person.id}</span> 
        <span>${person.firstName}</span>
        <button onclick="selectPerson(${person.id})">Edit</button>
    </div>
  `);

  allPersonsElement.innerHTML += personsHtml.join('');
};

window.fetchPersons = () => {
  // TODO Impl: fetch and render all persons
  fetch('persons', {
    method: 'GET'
  }).then(response => response.json())
    .then(persons => {
      allPersons = persons;
      renderPersons(allPersons);
    });

};

const updatePerson = (person) => {
  fetch(`persons/${person.id}`, {
    method: 'PUT',
    body: JSON.stringify(person),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(response => response.json())
    .then(updatedPerson => {
      allPersons = allPersons.map(p => {
        if (p.id === updatedPerson.id) {
          return updatedPerson;
        }
        return p;
      });
      renderPersons(allPersons);
    });
};

const addNewPerson = (person) => {
  fetch('persons', {
    method: 'POST',
    body: JSON.stringify(person),
    headers: {
      'Content-Type': 'application/json'
    }
  }).then(response => response.json())
    .then(newPerson => {
      allPersons.push(newPerson);
      renderPersons(allPersons);
    });
};

window.savePerson = (event) => {
  // TODO save a person, update the list of all persons and re-render the list
  event.preventDefault();

  const updatedFirstName = firstNameInput.value;

  if (selectedPerson) {
    const updatedPerson = {
      id: selectedPerson.id,
      firstName: updatedFirstName
    };
    updatePerson(updatedPerson);
  } else {
    const newPerson = {firstName: updatedFirstName};
    addNewPerson(newPerson);
  }
};

window.selectPerson = (personId) => {
  // TODO select a person(fetch it from back-end) and initialize the person form for editing
  selectedPerson = allPersons.find(person => person.id === personId);
  firstNameInput.value = selectedPerson.firstName;

};

const renderFile = (file) => {
  const fileHtml = `
    <a href="${file.fileDownloadPath}">${file.fileName}</a>
  `;

  const fileResultsElement = document.querySelector('#singleFileUploadResults');
  fileResultsElement.innerHTML += fileHtml;
};

window.onUploadFile = (event) => {
  // TODO get the first file from the input, append it in form data and send it to back-end. Then append a file link
  // to the list of uploaded files.
  event.preventDefault();

  const fileInputElement = document.querySelector('#singleFileUploadInput');
  const file = fileInputElement.files[0];
  const formData = new FormData();
  formData.append('file', file);

  fetch('uploadFile', {
    method: 'POST',
    body: formData
  }).then(response => response.json())
    .then(uploadedFile => renderFile(uploadedFile));
};
