// NOTE: fetch html page from backend and parse it

// Example HTML - This how the input of the parseHtml(text) should look like
const inputHTML = `
 <html lang="en">
   <body>
      <div class="someClass">
        <button id="minusButton" class="btn">-</button>
        0
        <button id="plusButton" class="btn">+</button>
      </div>
   </body>
 </html>
`;

// Example tokens - This is how the result of parseHtml(text) should look like
const outputTokens = [
  {startTag: true, value: 'html', type: 'TAG'},
  {name: 'lang', value: 'en', type: 'ATTRIBUTE'},
  {startTag: true, value: 'body', type: 'TAG'},
  {startTag: true, value: 'div', type: 'TAG'},
  {name: 'class', value: 'someClass', type: 'ATTRIBUTE'},
  
  {startTag: true, value: 'button', type: 'TAG'},
  {name: 'id', value: 'minusButton', type: 'ATTRIBUTE'},
  {name: 'class', value: 'btn', type: 'ATTRIBUTE'},
  {value: '-', type: 'TEXT'},
  {startTag: false, value: 'button', type: 'TAG'},
  
  {value: '0', type: 'TEXT'},
  
  {startTag: true, value: 'button', type: 'TAG'},
  {name: 'id', value: 'plusButton', type: 'ATTRIBUTE'},
  {name: 'class', value: 'btn', type: 'ATTRIBUTE'},
  {value: '+', type: 'TEXT'},
  {startTag: false, value: 'button', type: 'TAG'},
  
  {startTag: false, value: 'div', type: 'TAG'},
  {startTag: false, value: 'body', type: 'TAG'},
  {startTag: false, value: 'html', type: 'TAG'}
];

// Example tokens - This is how the result of constructDomTree(tokens) should look like
const domTreeExample = {
  document: { // Root node
    children: [
      {
        name: 'html',
        type: 'ELEMENT',
        children: [
          {
            name: 'body',
            type: 'ELEMENT',
            children: [
              {
                name: 'div',
                type: 'ELEMENT',
                attributes: [
                  {name: 'class', value: 'someClass'}
                ],
                children: [
                  {
                    name: 'button',
                    type: 'ELEMENT',
                    attributes: [
                      {name: 'class', value: 'someClass'},
                      {name: 'id', value: 'minusButton'}
                    ],
                    children: [
                      {type: 'TEXT', value: '-'}
                    ]
                  },
                  {type: 'TEXT', value: '+'},
                  {
                    name: 'button',
                    type: 'ELEMENT',
                    attributes: [
                      {name: 'class', value: 'someClass'},
                      {name: 'id', value: 'plusButton'}
                    ],
                    children: [
                      {type: 'TEXT', value: '+'}
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
};

/**
 * Creates HTML tokens of given HTML text.
 *
 * @param text HTML Text
 * @return HTML Tokens
 */
const parseHtml = text => {
  // TODO Implement
};

/**
 * Creates DOM Tree of given HTML tokens.
 *
 * @param tokens HTML Tokens
 * @return DOM Tree
 */
const constructDomTree = (tokens) => {
  // TODO Implement
};
