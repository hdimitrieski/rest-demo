let stompClient = null;

const conversation = document.querySelector('#conversation');
const connectButton = document.querySelector('#connect');
const disconnectButton = document.querySelector('#disconnect');
const messagesContainer = document.querySelector('#messages');
const sendButton = document.querySelector('#send');
const messageInput = document.querySelector('#message');
const fromInput = document.querySelector('#from');

function setConnected(connected) {
  if (connected) {
    conversation.style = 'display: block;';
    connectButton.setAttribute('disabled', 'true');
    disconnectButton.removeAttribute('disabled');
    messagesContainer.innerHTML = '';
  } else {
    conversation.style = 'display: none;';
    connectButton.removeAttribute('disabled');
    disconnectButton.setAttribute('disabled', 'true');
  }
}

function connect() {
  const socket = new SockJS('/demo-websocket');
  stompClient = Stomp.over(socket);
  stompClient.connect({}, function (frame) {
    setConnected(true);
    console.log('Connected: ' + frame);
    stompClient.subscribe('/topic/messages', function (message) {
      showMessage(JSON.parse(message.body));
    });
  });
}

function disconnect() {
  if (stompClient !== null) {
    stompClient.disconnect();
  }
  setConnected(false);
  console.log('Disconnected');
}

function sendMessage() {
  stompClient.send('/app/message', {}, JSON.stringify({
    from: fromInput.value,
    text: messageInput.value
  }));
}

function showMessage({from, text}) {
  messagesContainer.innerHTML += `<li>${from}: ${text}</li>`;
  messagesContainer.scrollIntoView(false)
}

document.querySelector('form').addEventListener('submit', function (e) {
  e.preventDefault();
});

connectButton.addEventListener('click', function (e) {
  e.preventDefault();
  connect();
});

disconnectButton.addEventListener('click',function (e) {
  e.preventDefault();
  disconnect();
});

sendButton.addEventListener('click',function (e) {
  e.preventDefault();
  sendMessage();
});

